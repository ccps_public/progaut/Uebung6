#ifndef PROGAUT_UEBUNG6_HISTOGRAM_HPP
#define PROGAUT_UEBUNG6_HISTOGRAM_HPP

#include <vector>

int getMostFrequentValue(const std::vector<int>& data);

#endif  // PROGAUT_UEBUNG6_HISTOGRAM_HPP
