#include "histogram.hpp"
#include <gtest/gtest.h>

TEST(HistogramTest, GetMostFrequentValue) {
    EXPECT_EQ(getMostFrequentValue({1, 2, 2, 3}), 2);
    EXPECT_EQ(getMostFrequentValue({10}), 10);
    EXPECT_EQ(getMostFrequentValue(
                {1, 2, 2, 2, 3, 3, 1, 3, 2, 8, 32, 1, 3, 1, 2, 4, 3, 2, 3, 3, 3}), 3);

    const int mfv = getMostFrequentValue({1, 2, 2, 3, 3});
    const bool correct = (mfv == 2) || (mfv == 3);
    EXPECT_TRUE(correct);
}