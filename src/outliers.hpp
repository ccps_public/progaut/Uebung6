#ifndef PROGAUT_UEBUNG6_OUTLIERS_HPP
#define PROGAUT_UEBUNG6_OUTLIERS_HPP

#include <vector>
#include <cstddef>
#include <cmath>

double findNMin(const std::vector<double>& data, size_t n);
double findNMax(const std::vector<double>& data, size_t n);
std::vector<double> removeOutliers(std::vector<double> data, size_t n);

#endif  // PROGAUT_UEBUNG6_OUTLIERS_HPP
