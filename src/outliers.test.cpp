#include "outliers.hpp"
#include <gtest/gtest.h>
#include <algorithm>

void allElementsInVector(const std::vector<double>& test, const std::vector<double>& correct) {
    ASSERT_EQ(test.size(), correct.size());
    std::vector<double> sorted_test = test;
    std::sort(sorted_test.begin(), sorted_test.end());
    for (size_t i = 0; i < correct.size(); ++i) {
        EXPECT_EQ(sorted_test[i], correct[i]);
    }
}

TEST(OutliersTest, FindNMin) {
    std::vector<double> data = {1, 2, 3, 4, 5};
    EXPECT_EQ(findNMin(data, 1), 1);
    EXPECT_EQ(findNMin(data, 2), 2);
    EXPECT_EQ(findNMin(data, 5), 5);

    std::vector<double> data2 = {4, 2, 1, 2, 3};
    EXPECT_EQ(findNMin(data2, 1), 1);
    EXPECT_EQ(findNMin(data2, 2), 2);
    EXPECT_EQ(findNMin(data2, 5), 4);
}

TEST(OutliersTest, FindNMax) {
    std::vector<double> data = {1, 2, 3, 4, 5};
    EXPECT_EQ(findNMax(data, 1), 5);
    EXPECT_EQ(findNMax(data, 2), 4);
    EXPECT_EQ(findNMax(data, 5), 1);

    std::vector<double> data2 = {4, 2, 1, 2, 3};
    EXPECT_EQ(findNMax(data2, 1), 4);
    EXPECT_EQ(findNMax(data2, 2), 3);
    EXPECT_EQ(findNMax(data2, 5), 1);
}

TEST(OutliersTest, RemoveOutliers) {
    std::vector<double> data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    std::vector<double> data2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 30, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    std::vector<double> correct = {0, 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 18, 18};
    std::vector<double> correct2 = {0, 0, 3, 4, 5, 6, 7, 8, 9, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 19};
    allElementsInVector(removeOutliers(data, 2), correct);
    allElementsInVector(removeOutliers(data2, 2), correct2);
}