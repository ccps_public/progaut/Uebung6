#ifndef PROGAUT_UEBUNG6_STATISTICS_HPP
#define PROGAUT_UEBUNG6_STATISTICS_HPP

#include <vector>
#include <cstddef>
size_t countValuesNearMean(const std::vector<double>& data);

#endif  // PROGAUT_UEBUNG6_STATISTICS_HPP
