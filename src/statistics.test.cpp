#include "statistics.hpp"
#include <gtest/gtest.h>

TEST(StatisticsTest, CountValuesNearMeanTest) {
    EXPECT_EQ(countValuesNearMean({1, 2, 3}), 1);
    EXPECT_EQ(countValuesNearMean({2, 3, 2, 1}), 2);
    EXPECT_EQ(countValuesNearMean({1, 4, 2, 3}), 0);
    EXPECT_EQ(countValuesNearMean({2.5, 2.5, 1, 4, 2, 3, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5}), 9);
}